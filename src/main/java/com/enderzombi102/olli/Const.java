package com.enderzombi102.olli;

import blue.endless.jankson.Jankson;
import blue.endless.jankson.JsonGrammar;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.util.Identifier;

import java.nio.file.Path;

public class Const {
	private Const() { }

	public static final Path CONFIG_LOCATION = FabricLoader.getInstance().getConfigDir().resolve( "OlliEnchants.json5" );
	public static final Jankson JANKSON = Jankson.builder().build();
	public static final JsonGrammar JSON5_GRAMMAR = JsonGrammar.builder()
		.withComments( true )
		.printUnquotedKeys( true )
		.printTrailingCommas( true )
		.build();
	public static final String ID = "olli";
	public static final String VERSION;

	public static Identifier getId( String path ) {
		return new Identifier( ID, path );
	}

	static {
		VERSION = FabricLoader.getInstance()
			.getModContainer( ID )
			.orElseThrow()
			.getMetadata()
			.getVersion()
			.getFriendlyString();
	}
}
