package com.enderzombi102.olli.enchant;

import com.enderzombi102.olli.config.Config;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SwordItem;

public class FireAffinity extends OlliEnchantment {
	public FireAffinity() {
		super( Rarity.COMMON, new EquipmentSlot[] { } );
		this.setMaxLevel( 5 );
	}

	@Override
	public float getAdditionalDamage( ItemStack stack, LivingEntity user, LivingEntity target, int level ) {
		float multiplier = Config.get().multiplier;
		return 1 + ( multiplier * level );
	}

	@Override
	public boolean isValidItem( Item item ) {
		return ( item instanceof SwordItem );
	}

	@Override
	public boolean canAccept( Enchantment other ) {
		return this != other;
	}
}
