package com.enderzombi102.olli.enchant;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public abstract class OlliEnchantment extends Enchantment {
	private int maxLevel = 1;

	protected OlliEnchantment( Rarity weight ) {
		this( weight, new EquipmentSlot[] { } );
	}

	protected OlliEnchantment( Rarity weight, EquipmentSlot[] slotTypes ) {
		super( weight, null, slotTypes );
	}

	public abstract boolean isValidItem( Item item );

	/**
	 * Called when an entity gets attacked by another with an item containing this enchant
	 * @param stack
	 * @param user
	 * @param target
	 * @param level
	 * @return the amount of additional damage
	 */
	public float getAdditionalDamage( ItemStack stack, LivingEntity user, LivingEntity target, int level ) {
		return 0;
	}

	@Override
	public boolean isAcceptableItem( ItemStack stack ) {
		return this.isValidItem( stack.getItem() );
	}

	@Override
	public abstract boolean canAccept( Enchantment other );

	@Override
	public int getMaxLevel() {
		return maxLevel;
	}

	protected void setMaxLevel( int maxLevel ) {
		this.maxLevel = maxLevel;
	}
}
