package com.enderzombi102.olli;

import com.enderzombi102.olli.registry.Enchantments;
import net.fabricmc.api.ModInitializer;

public class Olli implements ModInitializer {
	@Override
	public void onInitialize() {
		Enchantments.register();
	}
}
