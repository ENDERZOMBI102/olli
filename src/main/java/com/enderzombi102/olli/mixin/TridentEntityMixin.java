package com.enderzombi102.olli.mixin;

import net.minecraft.entity.projectile.TridentEntity;
import net.minecraft.util.hit.EntityHitResult;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(TridentEntity.class)
public class TridentEntityMixin {
	@Inject(
		method = "onEntityHit",
		at = @At(
			value = "INVOKE",
			shift = At.Shift.BEFORE,
			target = "Lnet/minecraft/enchantment/EnchantmentHelper;getAttackDamage(Lnet/minecraft/item/ItemStack;Lnet/minecraft/entity/EntityGroup;)F"
		)
	)
	public void onOnEntityHit( EntityHitResult entityHitResult, CallbackInfo ci ) {

	}
}
