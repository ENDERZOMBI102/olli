package com.enderzombi102.olli.config;

import blue.endless.jankson.api.SyntaxError;
import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;

import static com.enderzombi102.olli.Const.*;

public class Config {
	private static final Logger LOGGER = LogManager.getLogger( "JUno | Config" );
	private static @Nullable ConfigData DATA = null;

	public static @NotNull ConfigData get() {
		if ( DATA == null )
			load();
		return DATA;
	}

	public static void save() {
		try {
			Files.writeString( CONFIG_LOCATION, JANKSON.toJson( DATA ).toJson( JSON5_GRAMMAR ) );
		} catch ( IOException ex ) {
			LOGGER.error( "Failed to save config file!", ex );
		}
	}

	private static void load() {
		try {
			DATA = JANKSON.fromJson( Files.readString( CONFIG_LOCATION ), ConfigData.class );
		} catch ( SyntaxError e ) {
			throw new RuntimeException( e );
		} catch ( IOException e ) {
			LOGGER.warn( "Config file does not exist, will create a new one." );
			DATA = new ConfigData();
			save();
		}
	}
}
