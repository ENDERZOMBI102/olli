package com.enderzombi102.olli.registry;

import com.enderzombi102.olli.Const;
import com.enderzombi102.olli.enchant.FireAffinity;
import com.enderzombi102.olli.enchant.OlliEnchantment;
import net.minecraft.util.registry.Registry;

import java.util.HashMap;
import java.util.Map;

public class Enchantments {
	private static final Map<String, OlliEnchantment> ENCHANTMENTS = new HashMap<>() {{
		put( "fire_affinity", new FireAffinity() );
		put( "fire_affinity", new FireAffinity() );
	}};

	public static OlliEnchantment get( String id ) {
		return ENCHANTMENTS.get( id );
	}

	public static void register() {
		for ( var entry : ENCHANTMENTS.entrySet() )
			Registry.register(
				Registry.ENCHANTMENT,
				Const.getId( entry.getKey() ),
				entry.getValue()
			);
	}
}
